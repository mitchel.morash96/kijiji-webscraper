import requests
import re
import math
import pandas as pd
from bs4 import BeautifulSoup

class Post:
    def __init__(self, name, url, price, location, details, description):
        self.name = name
        self.url = url
        self.price = price
        self.location = location
        self.details = details
        self.description = description

def GetAllPostsOnPage(address):

    page = requests.get(address)
    soup = BeautifulSoup(page.content, 'html.parser')
    allPosts = soup.select(".search-item.regular-ad:not(.showcase)")

    for post in allPosts:
        temp = re.sub("\s+", " ", post.find("div", class_="details").text.strip())

        Postlist.append(
            Post(
                re.sub("\s+", " ", post.find("a", class_="title").text.strip()),
                re.sub("\s+", " ", post.find("a", class_="title").get("href")),
                re.sub("\s+", " ", post.find("div", class_="price").text.strip()),
                re.sub("\s+", " ", post.find("div", class_="location").text.strip()),
                re.sub("\s+", " ", post.find("div", class_="details").text.strip()),
                re.sub("\s+", " ", post.find("div", class_="description").text.strip()).replace(temp, "")
            )
        )

def PrintPostsFromList():
    for x in Postlist:
        print("Title:        " + x.name)
        print("Price:        " + x.price)
        print("Location:     " + x.location)
        print("Details:      " + x.details)
        print("Description:  " + x.description)
        print("Url:          " + x.url)
        print(" ")

def MakeExcelSheet():
    titleList = []
    priceList = []
    locationList = []
    detailsList = []
    descriptionList = []
    urlList = []

    for x in Postlist:
        titleList.append(x.name)
        priceList.append(x.price)
        locationList.append(x.location)
        detailsList.append(x.details)
        descriptionList.append(x.description)
        urlList.append("https://www.kijiji.ca"+x.url)

    df = pd.DataFrame({'Title': titleList,
                       'Price': priceList,
                       'Location': locationList,
                       'Details': detailsList,
                       'Description': descriptionList,
                       'Url': urlList})
    writer = pd.ExcelWriter('Vehicles.xlsx', engine='xlsxwriter')
    df.to_excel(writer, sheet_name='Sheet1', index=False)
    writer.save()

def GetNumOfPages(address):
    page = requests.get(address)
    soup = BeautifulSoup(page.content, 'html.parser')

    temp = re.sub("\s+", " ", soup.find("li", class_="highlight-l1-item").text.strip())
    postTotal = temp[temp.find("(")+1:temp.find(")")].replace(',', '')
    pages = math.ceil(int(postTotal)/NumOfPostPerPage)

    return pages

    
Postlist = []
staringSite = "https://www.kijiji.ca/b-cars-trucks/nova-scotia/c174l9002?for-sale-by=ownr"
NumOfPostPerPage = 40
numOfPosts = 0

numOfPages = GetNumOfPages(staringSite)
GetAllPostsOnPage(staringSite)

for i in range(2, numOfPages+1):
    address = "https://www.kijiji.ca/b-cars-trucks/nova-scotia/page-" + str(i) + "/c174l9002?for-sale-by=ownr"
    GetAllPostsOnPage(address)

#PrintPostsFromList()
MakeExcelSheet()
